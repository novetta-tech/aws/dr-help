#########################
# ASA EC2
#########################
resource "aws_instance" "asa" {
  count = var.enable_asa ? 1 : 0
  
  monitoring = true

  ami = "ami-0a5bb99a33a987f1e" # Cisco Adaptive Security Virtual Appliance (ASAv) - Standard Package
  instance_type         = "m4.large"
  iam_instance_profile  = aws_iam_instance_profile.base_ec2_instance_profile.name

  key_name = aws_key_pair.deployer.key_name

  subnet_id               = module.native_vpc.public_subnets[0]
  vpc_security_group_ids  = [aws_security_group.endpoint.id]

  root_block_device {
    volume_type = "gp2"
    volume_size = 50
    encrypted   = true
  }

 tags = {
    Name    = "asa"
    Project = local.project
  }
}
