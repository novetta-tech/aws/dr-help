#########################
# Domain Controllers EC2
#########################
resource "aws_instance" "dc" {
  count = var.enable_dc ? 2 : 0
  
  monitoring = true

  ami                   = "ami-047b577fb036bfa95" # Microsoft Windows Server 2012 R2 Base
  instance_type         = "m4.xlarge"
  iam_instance_profile  = aws_iam_instance_profile.base_ec2_instance_profile.name

  key_name = aws_key_pair.deployer.key_name
  get_password_data = true

  subnet_id               = module.native_vpc.private_subnets[count.index]
  private_ip              = cidrhost(module.native_vpc.private_subnets[count.index], count.index+10)
  vpc_security_group_ids  = [aws_security_group.endpoint.id]

  user_data = <<EOF
<powershell>
Invoke-WebRequest `
    https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/windows_amd64/AmazonSSMAgentSetup.exe `
    -OutFile $env:USERPROFILE\Desktop\SSMAgent_latest.exe
Start-Process `
    -FilePath $env:USERPROFILE\Desktop\SSMAgent_latest.exe `
    -ArgumentList "/S"
Restart-Service AmazonSSMAgent
</powershell>
EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = 100
    encrypted   = true
  }

 tags = {
    Name    = "dc-${count.index}"
    Project = local.project
  }
}
