################################################################################
#
# Type: IAM
# 
# Resources: 
#
################################################################################

#########################
# EC2 Base SSM 
#########################
data "aws_iam_policy" "AmazonSSMManagedInstanceCore" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy" "CloudWatchAgentServerPolicy" {
  arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

data "aws_iam_policy_document" "base_ec2_assume_role_policy" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com", "codedeploy.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "base_ec2_policy" {
  statement {
    effect = "Allow"
    actions = ["logs:CreateLogStream", "logs:PutLogEvents"]
    resources = ["*"]
  }

  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus",
      "ec2:ReportInstanceStatus"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "base_ec2" {
  name = "${local.name}-role"

  assume_role_policy = data.aws_iam_policy_document.base_ec2_assume_role_policy.json

  tags = {
    Name = "${local.name}-role"
    Project = local.project
  }
}

resource "aws_iam_policy" "base_ec2_policy" {
  name        = "${local.name}-policy"
  description = "Base EC2 policy"

  policy = data.aws_iam_policy_document.base_ec2_policy.json
}

resource "aws_iam_role_policy_attachment" "base_ec2_policy_attachment" {
  role        = aws_iam_role.base_ec2.name
  policy_arn  = aws_iam_policy.base_ec2_policy.arn
}

resource "aws_iam_role_policy_attachment" "base_ec2_ssm_policy" {
  role        = aws_iam_role.base_ec2.name
  policy_arn  = data.aws_iam_policy.AmazonSSMManagedInstanceCore.arn
}

resource "aws_iam_role_policy_attachment" "base_ec2_cloudagent_policy" {
  role        = aws_iam_role.base_ec2.name
  policy_arn  = data.aws_iam_policy.CloudWatchAgentServerPolicy.arn
}

resource "aws_iam_instance_profile" "base_ec2_instance_profile" {
  name  = "${local.name}-instance-profile"
  role  = aws_iam_role.base_ec2.name
}

#########################
# CloudEndure
#########################
data "aws_iam_policy_document" "cloudendure" {
  statement {
    sid = "CETerraform1"
    effect = "Allow"
    actions = ["ec2:CreateTags"]
    resources = ["arn:aws:ec2:*:*:*/*"]
    condition {
      test = "StringEquals"
      variable = "ec2:CreateAction"
      values = ["RunInstances"]
    }
  }

  statement {
    sid = "CETerraform2"
    effect = "Allow"
    actions = ["ec2:CreateTags"]
    resources = ["arn:aws:ec2:*:*:*/*"]
    condition {
      test = "StringEquals"
      variable = "ec2:CreateAction"
      values = ["CreateVolume"]
    }
  }

  statement {
    sid = "CETerraform3"
    effect = "Allow"
    actions = [
      "ec2:RevokeSecurityGroupIngress",
      "ec2:DetachVolume",
      "ec2:AttachVolume",
      "ec2:DeleteVolume",
      "ec2:TerminateInstances",
      "ec2:StartInstances",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:StopInstances"
    ]
    resources = [
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:security-group/*"
    ]
    condition {
      test = "StringLike"
      variable = "ec2:ResourceTag/Name"
      values = ["CloudEndure*"]
    }
  }

  statement {
    sid = "CETerraform4"
    effect = "Allow"
    actions = [
      "ec2:RevokeSecurityGroupIngress",
      "ec2:DetachVolume",
      "ec2:AttachVolume",
      "ec2:DeleteVolume",
      "ec2:TerminateInstances",
      "ec2:StartInstances",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:StopInstances"
    ]
    resources = [
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:security-group/*"
    ]
    condition {
      test = "StringLike"
      variable = "ec2:ResourceTag/CloudEndure creation time"
      values = ["*"]
    }
  }

  statement {
    sid = "CETerraform5"
    effect = "Allow"
    actions = [
      "ec2:DisassociateAddress",
      "ec2:CreateDhcpOptions",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:DeregisterImage",
      "ec2:DeleteSubnet",
      "ec2:DeleteSnapshot",
      "ec2:ModifySnapshotAttribute",
      "ec2:ModifyVolumeAttribute",
      "ec2:CreateVpc",
      "ec2:AttachInternetGateway",
      "ec2:GetConsoleScreenshot",
      "ec2:GetConsoleOutput",
      "elasticloadbalancing:DescribeLoadBalancer*",
      "ec2:CreateRoute",
      "ec2:CreateInternetGateway",
      "ec2:CreateSecurityGroup",
      "ec2:CreateSnapshot",
      "ec2:ModifyVpcAttribute",
      "ec2:ModifyInstanceAttribute",
      "ec2:ReleaseAddress",
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:AssociateDhcpOptions",
      "ec2:ImportKeyPair",
      "ec2:CreateTags",
      "ec2:RegisterImage",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:AssociateRouteTable",
      "ec2:CreateRouteTable",
      "ec2:DetachInternetGateway",
      "iam:ListInstanceProfiles",
      "ec2:AllocateAddress",
      "ec2:ReplaceNetworkAclAssociation",
      "ec2:CreateVolume",
      "kms:ListKeys",
      "ec2:Describe*",
      "ec2:DeleteVpc",
      "iam:GetUser",
      "ec2:CreateSubnet",
      "ec2:AssociateAddress",
      "ec2:DeleteKeyPair",
      "ec2:CreateNetworkAclEntry"
    ]
    resources = ["*"]
  }

  statement {
    sid = "MigrationHubConfig"
    effect = "Allow"
    actions = ["mgh:GetHomeRegion"]
    resources = ["*"]
  }

  statement {
    sid = "CETerraform6"
    effect = "Allow"
    actions = [
      "ec2:RevokeSecurityGroupIngress",
      "mgh:CreateProgressUpdateStream",
      "kms:Decrypt",
      "kms:Encrypt",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:DeleteDhcpOptions",
      "ec2:RunInstances",
      "kms:DescribeKey",
      "kms:CreateGrant",
      "ec2:DeleteNetworkAclEntry",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*"
    ]
    resources = [
      "arn:aws:mgh:*:*:progressUpdateStream/*",
      "arn:aws:ec2:*:*:subnet/*",
      "arn:aws:ec2:*:*:key-pair/*",
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:security-group/*",
      "arn:aws:ec2:*:*:network-acl/*",
      "arn:aws:ec2:*:*:placement-group/*",
      "arn:aws:ec2:*:*:vpc/*",
      "arn:aws:ec2:*:*:network-interface/*",
      "arn:aws:ec2:*::image/*",
      "arn:aws:ec2:*:*:snapshot/*",
      "arn:aws:kms:*:*:key/*"
    ]
  }

  statement {
    sid = "CETerraform7"
    effect = "Allow"
    actions = [
      "ec2:CreateTags",
      "mgh:ImportMigrationTask",
      "mgh:AssociateCreatedArtifact",
      "mgh:NotifyMigrationTaskState",
      "mgh:DisassociateCreatedArtifact",
      "mgh:PutResourceAttributes"
    ]
    resources = [
      "arn:aws:mgh:*:*:progressUpdateStream/*/migrationTask/*",
      "arn:aws:ec2:*:*:subnet/*",
      "arn:aws:ec2:*::network-interface/*",
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*::snapshot/*",
      "arn:aws:ec2:*:*:security-group/*",
      "arn:aws:ec2:*::image/*"
    ]
  }

  statement {
    sid = "CETerraform8"
    effect = "Allow"
    actions = ["ec2:Delete"]
    resources = [
      "arn:aws:ec2:*:*:route-table/*",
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:security-group/*",
      "arn:aws:ec2:*:*:internet-gateway/*"
    ]
    condition {
      test = "StringLike"
      variable = "ec2:ResourceTag/Name"
      values = ["CloudEndure*"]
    }
  }

  statement {
    sid = "CETerraform9"
    effect = "Allow"
    actions = ["ec2:Delete*"]
    resources = [
      "arn:aws:ec2:*:*:route-table/*",
      "arn:aws:ec2:*:*:dhcp-options/*",
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:security-group/*",
      "arn:aws:ec2:*:*:internet-gateway/*"
    ]
    condition {
      test = "StringLike"
      variable = "ec2:ResourceTag/CloudEndure creation time"
      values = ["*"]
    }
  }
}

resource "aws_iam_user" "cloudendure" {
  name = "cloudendure"
  path = "/dr/"

  force_destroy = true

  tags = {
    Project = local.project
  }
}

resource "aws_iam_access_key" "cloudendure" {
  user = aws_iam_user.cloudendure.name
}

resource "aws_iam_policy" "cloudendure" {
  name    = "cloudendure-policy"
  description = "CloudEndure user policy for disaster recovery."
  policy      = data.aws_iam_policy_document.cloudendure.json
}

resource "aws_iam_user_policy_attachment" "cloudendure" {
  user        = aws_iam_user.cloudendure.name
  policy_arn  = aws_iam_policy.cloudendure.arn
}