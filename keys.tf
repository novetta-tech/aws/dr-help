data "local_file" "public_key" {
    filename = var.public_key_path
}

resource "aws_key_pair" "deployer" {
  key_name_prefix = "deployer"
  public_key      = data.local_file.public_key.content
}
