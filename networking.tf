################################################################################
#
# Type: NETWORKING
# 
# Resources: 
#
################################################################################
resource "aws_eip" "this" {
  count = length(var.availability_zones)
  vpc   = true
}

#########################
# VPC
#########################
module "native_vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.21.0"

  name = local.name
  cidr = var.vpc_cidr

  azs             = var.availability_zones
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets

  enable_nat_gateway  = true
  single_nat_gateway  = false
  reuse_nat_ips       = true 
  external_nat_ip_ids = "${aws_eip.this.*.id}"

  enable_dns_hostnames= true
  enable_dns_support  = true

  enable_vpn_gateway  = true
  amazon_side_asn     = 7224

  propagate_private_route_tables_vgw = true

  customer_gateways = {
    IP1 = {
      bgp_asn     = 64512
      ip_address  = "199.87.3.197"
      type        = "ipsec.1"
    }
  }

  # VPC endpoint for SSM
  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = true
  ssm_endpoint_security_group_ids  = [aws_security_group.endpoint.id]

  # VPC endpoint for SSMMESSAGES
  enable_ssmmessages_endpoint              = true
  ssmmessages_endpoint_private_dns_enabled = true
  ssmmessages_endpoint_security_group_ids  = [aws_security_group.endpoint.id]

  # VPC Endpoint for EC2
  enable_ec2_endpoint              = true
  ec2_endpoint_private_dns_enabled = true
  ec2_endpoint_security_group_ids  = [aws_security_group.endpoint.id]

  # VPC Endpoint for EC2MESSAGES
  enable_ec2messages_endpoint              = true
  ec2messages_endpoint_private_dns_enabled = true
  ec2messages_endpoint_security_group_ids  = [aws_security_group.endpoint.id]

  # VPC endpoint for API gateway
  enable_apigw_endpoint              = true
  apigw_endpoint_private_dns_enabled = true
  apigw_endpoint_security_group_ids  = [aws_security_group.endpoint.id]

  public_subnet_tags = {
    Name    = "public-${local.name}"
    Project = local.project
  }

  private_subnet_tags = {
    Name    = "private-${local.name}"
    Project = local.project
  }

  private_route_table_tags = {
    Name    = "private-${local.name}"
    Project = local.project
  }

  vpc_endpoint_tags = {
    Project  = local.project
    Endpoint = "true"
  }

  tags = {
    Name    = local.name
    Project = local.project
  }
}

resource "aws_flow_log" "this" {
  vpc_id                = module.native_vpc.vpc_id
  log_destination       = aws_s3_bucket.flow_logs.arn
  log_destination_type  = "s3"
  traffic_type          = "ALL"
}

#########################
# Endpoint Security Group
#########################
resource "aws_security_group" "endpoint" {
  name        = "${local.name}-vpc-endpoints"
  description = "Allow vpc endpoint traffic."
  vpc_id      = module.native_vpc.vpc_id

  ingress {
    description = "Allow vpc endpoint traffic."
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["10.255.0.0/16"]
  }

  ingress {
    description = "Allow ssh traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  ingress {
    description = "Allow all traffic from self"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    self        = true
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "${local.name}-vpc-endpoints"
    Project = local.project
  }
}

#########################
# VPN Connection
#########################
resource "aws_vpn_connection" "this" {
  type                = "ipsec.1"
  vpn_gateway_id      = module.native_vpc.vgw_id
  customer_gateway_id = module.native_vpc.cgw_ids[0]
  static_routes_only  = false
}

resource "aws_route" "private_vgw" {
  count = length(var.availability_zones)

  destination_cidr_block  = "10.0.0.0/8"
  route_table_id          = element(module.native_vpc.private_route_table_ids, count.index)
  gateway_id              = module.native_vpc.vgw_id
}

#########################
# CloudEndure Security Group
#########################
resource "aws_security_group" "cloudendure" {
  name        = "${local.name}-cloudendure"
  description = "Allow CloudEndure replication."
  vpc_id      = module.native_vpc.vpc_id

  ingress {
    description = "Allow cloudendure replication traffic."
    from_port   = 1500
    to_port     = 1500
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "${local.name}-cloudendure"
    Project = local.project
  }
}

#########################
# Oracle Security Group
#########################
resource "aws_security_group" "oracle" {
  name        = "${local.name}-oracle"
  description = "Allow Oracle traffic."
  vpc_id      = module.native_vpc.vpc_id

  ingress {
    description = "Allow oracle traffic traffic."
    from_port   = 1521
    to_port     = 1521
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "${local.name}-cloudendure"
    Project = local.project
  }
}