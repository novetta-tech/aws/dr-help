#########################
# Oracle EC2
#########################
resource "aws_instance" "oracle" {
  count = var.enable_oracle ? 1 : 0
  
  monitoring = true

  ami                   = "ami-0a54aef4ef3b5f881" # Red Hat Enterprise Linux 8 (HVM)
  instance_type         = "m5.2xlarge"
  iam_instance_profile  = aws_iam_instance_profile.base_ec2_instance_profile.name

  subnet_id               = module.native_vpc.private_subnets[0]
  vpc_security_group_ids  = [aws_security_group.endpoint.id]

  user_data = <<EOF
#!/bin/bash
set +x

sudo dnf install -y https://s3.us-east-2.amazonaws.com/amazon-ssm-us-east-2/latest/linux_amd64/amazon-ssm-agent.rpm
sudo systemctl status amazon-ssm-agent
EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = 100
    encrypted   = true
  }

 tags = {
    Name    = "oracle"
    Project = local.project
  }
}
