output "dc_password" {
  value = aws_instance.dc[0]password_data
  value = concat(aws_instance.dc.*.id, [""])[0]
}