#########################
# Palo Alto EC2
#########################
resource "aws_instance" "palo_alto" {
  count = var.enable_pa ? 1 : 0

  monitoring = true

  ami = "ami-0e912cb1b3d5117b2" # Palo Alto Networks VM-300 Bundle 2
  instance_type         = "m5n.xlarge"
  iam_instance_profile  = aws_iam_instance_profile.base_ec2_instance_profile.name

  key_name = aws_key_pair.deployer.key_name

  subnet_id               = module.native_vpc.public_subnets[0]
  vpc_security_group_ids  = [aws_security_group.endpoint.id]

  root_block_device {
    volume_type = "gp2"
    volume_size = 50
    encrypted   = true
  }

 tags = {
    Name    = "palo-alto"
    Project = local.project
  }
}
