################################################################################
#
# Type: STORAGE
# 
# Resources: 
#
################################################################################

#########################
# Flow Logs Bucket
#########################
resource "aws_s3_bucket" "flow_logs" {
  bucket  = "ccx-${data.aws_caller_identity.current.account_id}-${var.region}-flowlogs"
  acl     = "private"

  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "flow_logs" {
  bucket = aws_s3_bucket.flow_logs.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#########################
# S3 Transfer Bucket
#########################
resource "aws_s3_bucket" "transfer" {
  bucket  = "ccx-${data.aws_caller_identity.current.account_id}-${var.region}-transfer"
  acl     = "private"

  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "transfer" {
  bucket = aws_s3_bucket.transfer.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket" "log_bucket" {
  bucket  = "ccx-${data.aws_caller_identity.current.account_id}-${var.region}-log-bucket"
  acl     = "log-delivery-write"

  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "log_bucket" {
  bucket = aws_s3_bucket.log_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#########################
# FSx for Windows
#########################
resource "aws_fsx_windows_file_system" "this" {
  count = var.enable_fsx ? 1 : 0

  deployment_type     = "SINGLE_AZ_1"
  storage_type        = "SSD"
  subnet_ids          = [module.native_vpc.private_subnets[0]]

  storage_capacity    = 5000
  throughput_capacity = 1024

  automatic_backup_retention_days = 0

  self_managed_active_directory {
    dns_ips     = ["10.0.0.111", "10.0.0.222"]
    domain_name = "ccx.carecentrix.com"
    password    = "avoid-plaintext-passwords"
    username    = "Admin"
  }

  tags = {
    Name = local.name
    Project = local.project
  }
}

#########################
# FSx DataSync
#########################
resource "aws_datasync_location_fsx_windows_file_system" "this" {
  count = var.enable_fsx ? 1 : 0

  fsx_filesystem_arn  = aws_fsx_windows_file_system.this[count.index].arn
  subdirectory        = "/"
  
  user                = "SomeUser"
  password            = "SuperSecretPassw0rd"
  domain              = "ccx.carecentrix.com"
  security_group_arns = [aws_security_group.endpoint.id]

  tags = {
    Name = local.name
    Project = local.project
  }
}
