variable "region" {
  description = "The AWS region resources will be created in."
  type        = string
  default     = "us-east-2"
}

variable "vpc_cidr" {
  description = "The main account VPC CIDR block."
  type        = string
  default     = "10.255.8.0/21"
}

variable "availability_zones" {
  description = "The availability zone in the vpc."
  type        = list(string)
  default     = ["us-east-2a", "us-east-2b"]
}

variable "public_subnets" {
  description = "The main accounts VPC public subnet cidr."
  type        = list(string)
  default     = ["10.255.13.0/24", "10.255.14.0/24"]
}

variable "private_subnets" {
  description = "The main accounts VPC private subnet cidr."
  type        = list(string)
  default     = ["10.255.9.0/24", "10.255.10.0/24"]
}

variable "public_key_path" {
  default = "~/.ssh/aws_rsa.pub"
}

variable "enable_fsx" {
  default = false
}

variable "enable_pa" {
  default = false
}

variable "enable_asa" {
  default = false
}

variable "enable_oracle" {
  default = false
}

variable "enable_dc" {
  default = false
}